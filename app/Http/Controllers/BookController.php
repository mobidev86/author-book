<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Author;
use App\Models\AuthorBook;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    public function index(){
        $authors = Author::get();
        $books = Book::get();
        $topAuthors = [];

        $topAuthors = Author::select("authors.author_name", DB::raw('COUNT(*) as number_of_books'))
        ->join("author_book", "authors.id", "=", "author_book.author_id")
        ->orderBy("number_of_books","desc")
        ->groupBy("authors.author_name")
        ->limit(3)
        ->get();

        return view('book.index',compact('authors','books','topAuthors'));
    }

    
}