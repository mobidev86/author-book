<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Author;
use App\Models\AuthorBook;

class ApiController extends Controller
{
    public function import(Request $request)
    {
        $requestData = $request->all();

        try {
            if (count($requestData) == 0) {
                return response()->json(['status' => 404, 'message' => 'No data found!']);
            }
            // Insert authors
            $authors = [];
            $authorsName = [];
            $checkedAuthors = [];

            foreach ($requestData as $singleData) {
                $authorsData = isset($singleData['authors']) ? ($singleData['authors']) : [];
                if ($authorsData) {
                    foreach ($authorsData as  $singleAuthor) {
                        if (!in_array(trim($singleAuthor), $authors) && !empty(trim($singleAuthor))) {
                            $authors[] = trim($singleAuthor);
                            $authorsName[]['author_name'] = trim($singleAuthor);
                        }
                    }
                }
            }

            $check_authors = Author::pluck('author_name')->toArray();

            if (count($authors) > 0) {
                if (count($check_authors) == 0) {
                    Author::insert($authorsName);
                } else {
                    foreach ($authors as $author_name) {
                        if (!in_array(trim($author_name), $check_authors) && !empty(trim($author_name))) {
                            $checkedAuthors[]['author_name'] = trim($author_name);
                        }
                    }
                    if (count($checkedAuthors) > 0) {
                        Author::insert($checkedAuthors);
                    }
                }
            }
            //get book to check exist or not
            $getBooks = Book::pluck('title')->toArray();

            foreach ($requestData as $book) {
                $bookArray = [
                    'title' => isset($book['title']) ? ($book['title']) : '',
                    'isbn' => isset($book['isbn']) ? ($book['isbn']) : '',
                    'pageCount' => isset($book['pageCount']) ? ($book['pageCount']) : '',
                    'publishedDate' => isset($book['publishedDate']['$date']) ? (date('Y-m-d H:i:s', strtotime($book['publishedDate']['$date']))) : NULL,
                    'thumbnailUrl' => isset($book['thumbnailUrl']) ? ($book['thumbnailUrl']) : '',
                    'shortDescription' => isset($book['shortDescription']) ? ($book['shortDescription']) : '',
                    'longDescription' => isset($book['longDescription']) ? ($book['longDescription']) : '',
                    'status' => isset($book['status']) ? ($book['status']) : '',
                    'categories' => isset($book['categories']) ? (json_encode($book['categories'])) : '',
                ];

                if (isset($book['title']) && !in_array($book['title'], $getBooks)) {
                    // create book
                    $bookRow = Book::create($bookArray);
                    if (isset($book['authors']) && is_array($book['authors'])) {
                        foreach ($book['authors'] as $singleAuthor) {
                            //get author ID
                            $authorRow = Author::where('author_name', trim($singleAuthor))->first();

                            if (isset($authorRow->id)) {
                                AuthorBook::create([
                                    'book_id' => $bookRow->id,
                                    'author_id' => $authorRow->id,
                                ]);
                            }
                        }
                    }
                }
            }
            return response()->json(['status' => 200, 'message' => 'inserted!']);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
