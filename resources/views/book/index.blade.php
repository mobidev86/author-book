<!DOCTYPE html>
<html lang="en">

<head>
    <title>Books</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .dataTables_filter{
            display: none;
        }
    </style>
</head>

<body>
    <div class="container">
        <div>
            <h2>Books</h2>
        </div>
        <div style="margin-top: 10px;">
            <p style="font-weight: 700;">Top 3 Authors:</p>
            <ul>
                @foreach($topAuthors as $key => $three)
                    <li><a href="javascript:;" class="top-three" data-name="{{ $three->author_name }}">{{ $three->author_name }}</a></li>
                @endforeach
            </ul>
        </div>

        <div style="margin-bottom: 10px;">
            <div class="row">
                <div>
                    <select class="js-example-basic-single" name="authors">
                        <option value="">Search by author</option>
                        @foreach($authors as $a)
                            <option value="{{ $a->id }}"> {{ $a->author_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div style="margin-left:10px;">
                    <button type="button" class="btn btn-dark btn-sm reset">Reset</button>
                </div>
            </div>
        </div>


        <table id="books" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>Title</th>
                    <th>ISBN</th>
                    <th>Page Count</th>
                    <th>Authors</th>
                </tr>
            </thead>
            <tbody>
                @foreach($books as $key=> $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td><img src="{{ $value->thumbnailUrl }}" width="80px;"></td>
                    <td>{{ $value->title }}</td>
                    <td>{{ $value->isbn }}</td>
                    <td>{{ $value->pageCount }}</td>
                    <td>
                        @php
                            $auth = "";
                        @endphp
                        @foreach($value->author as $v)
                            @php
                                $auth .= $v->author_name.", ";
                            @endphp
                        @endforeach
                        {{rtrim($auth, ", ")}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            var table = $('#books').DataTable({
                aLengthMenu: [
                    [10, 25, 50, 100, 200, -1],
                    [10, 25, 50, 100, 200, "All"]
                ],
                iDisplayLength: 10,
            });


            $('.js-example-basic-single').select2({
                placeholder: "Search by author",
                allowClear: true
            });
            $('.js-example-basic-single').on('select2:select', function (e) {
                var data = e.params.data;
                var id = data.id;
                table.search(data.text).draw();
            });

            $('.top-three').on('click', function (e) {
                var name = $(this).attr("data-name");
                table.search(name).draw();
            });

            $('.reset').on('click', function (e) {
                table.search("").draw();
                $(".js-example-basic-single").val('').trigger('change')
            });
        });
    </script>

</body>

</html>