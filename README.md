# Getting started

Clone the repository

    git clone https://gitlab.com/mobidev86/author-book.git

Switch to the repo folder

    cd author-book

Install all the dependencies using composer

    composer install
    
Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

Run the import datasource (**Should be JSON format)

    http://localhost:8000/api/import 